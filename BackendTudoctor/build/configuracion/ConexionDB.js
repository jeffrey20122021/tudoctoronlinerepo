"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const ConexionDB = () => {
    // variables de configuracion
    const urlConexion = String(process.env.DB_MONGO);
    console.log(urlConexion);
    (0, mongoose_1.connect)(urlConexion)
        .then(() => {
        console.log("conectado a", urlConexion);
    })
        .catch((elError) => {
        console.log("no se puede conectar a mongo", elError);
    });
};
exports.default = ConexionDB;
