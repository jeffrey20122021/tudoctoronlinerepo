"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class UsuarioEntidad {
    constructor(nom, pass, correo, fecha, est, codP) {
        this.nombreUsuario = nom;
        this.claveUsuario = pass;
        this.correoUsuario = correo;
        this.fechaUsuario = fecha;
        this.estadoUsuario = est;
        this.codPerfil = codP;
    }
}
exports.default = UsuarioEntidad;
